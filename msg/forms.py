from . import models
from django import forms

class CreateMessageForm(forms.ModelForm):
    class Meta:
        model = models.Message
        fields = [
            "message",
        ]

class CreateCommentForm(forms.ModelForm):
    class Meta:
        model = models.Comment
        fields=[
            "comment",
        ]