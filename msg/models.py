from django.db import models
from django.contrib.auth.models import User

class Message(models.Model):
    
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE,blank=True, null=True)
    def __str__(self):
        return "{} - {}".format(self.author, self.snipped_message())
    
    def snipped_message(self):
        if len(self.message) > 20 :
            return self.message[:20] + " ..."
        return self.message

class Comment(models.Model):
    comment = models.TextField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    message = models.ForeignKey(Message, on_delete=models.CASCADE, blank=True, null=True, related_name="comments")
    author = models.ForeignKey(User, default=None, on_delete=models.CASCADE,blank=True, null=True)
    def __str__(self):
        return "{} - {}".format(self.author, self.comment)
    
