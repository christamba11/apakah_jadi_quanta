from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Message, Comment
from .import forms
# Create your views here.
def message_list(request):
    messages = Message.objects.all()
    message_form = forms.CreateMessageForm()
    comment_form = forms.CreateCommentForm()
    response = {
        'msgs' : messages,
        'form' : message_form,
        'comment_form': comment_form
    }
    return render(request, 'msg/message_list.html', response)

@login_required(login_url="/accounts/login")
def message_create(request):
    message_form= forms.CreateMessageForm(request.POST)
    if request.method == "POST" and message_form.is_valid():
        instance = message_form.save(commit=False)
        instance.author = request.user
        instance.save()
    return redirect('msg:message_list')

def comment_create(request, message_id):
    comment_form= forms.CreateCommentForm(request.POST)
    message = Message.objects.get(pk=message_id)
    if request.method == "POST" and comment_form.is_valid():
        instance = comment_form.save(commit=False)
        instance.author = request.user
        instance.message= message
        instance.save()        
    return redirect('msg:message_list')
